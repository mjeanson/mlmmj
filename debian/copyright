Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mlmmj
Upstream-Contact: MLMMJ <mlmmj@mlmmj.org>
Source: http://www.mlmmj.org

Files: *
Copyright: (c) 2002, 2003, 2004 Mads Martin Joergensen
 (c) 2004-2009, Morten K. Poulsen <morten@afdelingp.dk>
 (c) 2009-2011, Ben Schmidt <mail_ben_schmidt@yahoo.com.au>
 (c) 2004, Christoph Thiel <ct@kki.org>
 (c) 2004, Christian Laursen <christian@pil.dk>
 (c) 2010-2012, Thomas Goirand <zigo@debian.org>
 (c) 2023 Baptiste Daroussin <bapt@FreeBSD.org>
 (c) 2023 Franky Van Liedekerke <franky at e-dynamics dot be>
License: Expat

Files: debian/*
Copyright: (c) 2004-2007, Søren Boll Overgaard <boll@debian.org>
 (c) 2007, Christian Perrier <bubulle@debian.org>
 (c) 2007, Matej Vela <vela@debian.org>
 (c) 2007-2010, Daniel Walrond <debian@djw.org.uk>
 (c) 2008, Marc 'HE' Brockschmidt <he@debian.org>
 (c) 2010-2012, Thomas Goirand <zigo@debian.org>
 (c) 2016-2017, Christopher Knadle <Chris.Knadle@coredump.us>
 (c) 2024, Michael Jeanson <mjeanson@debian.org>
License: Expat

Files: src/find_email_adr.c
Copyright: (c) 1980, 1993, The Regents of the University of California
License: BSD-4-clause

Files: contrib/foot_filter/foot_filter.c
Copyright: (c) 2010 Ben Schmidt <mail_ben_schmidt at yahoo.com.au>
License: MPL-2.0

Files: contrib/pymime/*
Copyright: (c) 2011 Alexander Werner
License: GPL-3.0+

Files: contrib/receivestrip/*
Copyright: (c) 2007 Sascha Sommer <ssommer at suse.de>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: BSD-4-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 3. All advertising materials mentioning features or use of this software must
 display the following acknowledgement: This product includes software
 developed by the University of California, Berkeley and its contributors.
 .
 4. Neither the name of the University nor the names of its contributors may be
 used to endorse or promote products derived from this software without
 specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF	SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: MPL-2.0
 This Source Code Form is subject to the terms of the Mozilla Public License,
 v. 2.0. If a copy of the MPL was not distributed with this file, You can
 obtain one at https://mozilla.org/MPL/2.0/.
 .
 On Debian systems, the complete text of the Mozilla Public Licnese
 Version 2.0 can be found in "/usr/share/common-licenses/MPL-2.0". 

License: GPL-3.0+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in "/usr/share/common-licenses/GPL-3".
