# translation of mlmmj's po-debconf to Spanish
#
# This file is distributed under the same license as the mlmmj package.
#
# Changes: 
# - Initial translation
#   José Ignacio Méndez González <jose_ignacio_aky@hotmail.com>, 2006
# - Revision
#   David Martínez Moreno
# - Review and update
#   Javier Fernández-Sanguino , 2006
#
#  Traductores, si no conoce el formato PO, merece la pena leer la 
#  documentación de gettext, especialmente las secciones dedicadas a este
#  formato, por ejemplo ejecutando:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
# Equipo de traducción al español, por favor lean antes de traducir
# los siguientes documentos:
#
# - El proyecto de traducción de Debian al español
#   http://www.debian.org/intl/spanish/
#   especialmente las notas y normas de traducción en
#   http://www.debian.org/intl/spanish/notas
#
# - La guía de traducción de po's de debconf:
#   /usr/share/doc/po-debconf/README-trans
#   o http://www.debian.org/intl/l10n/po-debconf/README-trans
#
# Si tiene dudas o consultas sobre esta traducción consulte con el último
# traductor (campo Last-Translator) y ponga en copia a la lista de
# traducción de Debian al español (<debian-l10n-spanish@lists.debian.org>)
#
msgid ""
msgstr ""
"Project-Id-Version: mlmmj 1.2.11-7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-10-19 01:07+0200\n"
"PO-Revision-Date: 2006-12-28 13:50+0100\n"
"Last-Translator: Javier Fernández-Sanguino <jfs@debian.org>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../mlmmj.templates:1001
msgid "Remove mlmmj lists on purge?"
msgstr "¿Desea eliminar las listas mlmmj al purgar?"

#. Type: boolean
#. Description
#: ../mlmmj.templates:1001
msgid ""
"Removing mlmmj on purge includes the removal of all subscriber lists, "
"archives and configuration options for all lists currently stored."
msgstr "Si elimina mlmmj al purgar esto incluirá la eliminación de todas las listas de suscripción, archivos y opciones de configuración para todas las listas actualmente guardadas."

#. Type: boolean
#. Description
#: ../mlmmj.templates:1001
msgid ""
"Accepting here basically means that everything under /var/spool/mlmmj and /"
"etc/mlmmj/lists will be removed when this package is purged. Also please "
"note that any changes you might have made to /etc/aliases will not be unmade "
"automatically when this package is removed. (A notice will be displayed "
"however, to remind you to clean up your aliases.)"
msgstr "Si responde afirmativamente todo lo que esté bajo «/var/spool/mlmmj» y «/etc/mlmmj/lists» se eliminará cuando se purgue el paquete. Sepa también que cualquier cambio que pudiera haber hecho en /etc/aliases no se deshará de forma automática cuando se elimine este paquete. No obstante, se le mostrará un mensaje para recordarle que limpie su fichero «aliases»."
