# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE 'S COPYRIGHT HOLDER
# This file is distributed under the same license as the  package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: mlmmj\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-10-02 04:31+0200\n"
"PO-Revision-Date: 2007-10-25 19:38+0100\n"
"Last-Translator: Bart Cornelis <cobaco@skolelinux.no>\n"
"Language-Team: debian-l10n-dutch <debian-l10n-dutch@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Dutch\n"

#. Type: boolean
#. Description
#: ../mlmmj.templates:1001
msgid "Remove mlmmj lists on purge?"
msgstr "Wilt u de mlmmj-lijsten verwijderen wanneer dit pakket gewist wordt?"

#. Type: boolean
#. Description
#: ../mlmmj.templates:1001
msgid "Removing mlmmj on purge includes the removal of all subscriber lists, archives and configuration options for all lists currently stored."
msgstr "Als u het pakket 'mlmmj' wist worden alle abonneelijsten, archieven, en configuratie-opties van de momenteel opgeslagen lijsten verwijderd."

#. Type: boolean
#. Description
#: ../mlmmj.templates:1001
msgid "Accepting here basically means that everything under /var/spool/mlmmj and /etc/mlmmj/lists will be removed when this package is purged. Also please note that any changes you might have made to /etc/aliases will not be unmade automatically when this package is removed. (A notice will be displayed however, to remind you to clean up your aliases.)"
msgstr "Als u dit aanvaard betekend dit dat alles onder /var/spool/mlmmj en /etc/mlmmj/lists verwijderd wordt wanneer dit pakket gewist wordt. Merk op dat veranderingen die u eventueel in /etc/aliases maakt niet automatisch verwijderd worden wanneer dit pakket verwijderd wordt (Er wordt wel een bericht weergegeven om u eraan te herinneren dat uw aliases opgeruimd moeten worden)."

